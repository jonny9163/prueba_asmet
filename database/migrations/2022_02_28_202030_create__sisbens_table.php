<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSisbensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sisbens', function (Blueprint $table) {
            $table->bigIncrements('id_sisben');
            $table->string('nivel_sisben');
            $table->string('puntaje');
            $table->unsignedBigInteger('id_afiliado')->nullable();

            $table->foreign('id_afiliado')->references('id_afiliado')->on('afiliados');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sisbens');
    }
}
