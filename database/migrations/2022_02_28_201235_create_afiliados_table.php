<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAfiliadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('afiliados', function (Blueprint $table) {
            $table->bigIncrements('id_afiliado');
            $table->string('n_identificacion')->unique();
            $table->string('Nombre');
            $table->string('tipo_documento');
            $table->string('direccion');
            $table->string('telefono');
            $table->dateTime('fecha_nacimiento');
            $table->dateTime('fecha_afiliacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('afiliados');
    }
}
