@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-18">
			<div class="card" style="width:1200px;overflow-x:hidden;">
				<div class="card-header" align="center" style="  color: black;border-radius: 10px; height: 40px">
					<h4>{{ __('Crear Afiliados') }}</h4>
				</div>
				<form method="POST" action="{{ route('afiliados.store') }}"  role="form">
							{{ csrf_field() }}
					<div class="row" align="center">
						<div class="col-sm">
							<label for="tipo_identificacion">Tipo de identificación</label>
							<select id="tipo_identificacion" type="text" name="tipo_identificacion">
								<option value="registro civil">Registro civil</option>
								<option value="Ti">Tarjeta de identidad</option>
								<option value="CC">Cedula de ciudadania</option>
							</select>
						</div>
					</div>
					<div class="row" align="center">
						<div class="col-sm">
							<label for="n_identificacion">Numero de identificación</label>
							<div class="input-group" style="width: 400px">
								<input type="text" class="form-control" id="n_identificacion" name="n_identificacion">
							</div>
						</div>
					</div>

					<div class="row" align="center">
						<div class="col-sm">
							<label for="nombre">Nombre afiliado</label>
							<div class="input-group" style="width: 400px">
								<input type="text" class="form-control" id="nombre" name="nombre">
							</div>
						</div>
					</div>

					<div class="row" align="center">
						<div class="col-sm">
							<label for="direccion">Dirección del afiliado</label>
							<div class="input-group" style="width: 400px">
								<input type="text" class="form-control" id="direccion" name="direccion">
							</div>
						</div>
					</div>

					<div class="row" align="center">
						<div class="col-sm">
							<label for="telefono">Telefono del afiliado</label>
							<div class="input-group" style="width: 400px">
								<input type="text" class="form-control" id="telefono" name="telefono">
							</div>
						</div>
					</div>

					<div class="row" align="center">
						<div class="col-sm">
							<label for="fecha_nacimiento">Fecha de nacimiento</label>
							<div class="input-group" style="width: 400px">
								<input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="aaaa-mm-dd">
							</div>
						</div>
					</div>

					<div class="row" align="center">
						<div class="col-sm">
							<label for="fecha_afiliacion">Fecha de afiliación</label>
							<div class="input-group" style="width: 400px">
								<input type="text" class="form-control" id="fecha_afiliacion" name="fecha_afiliacion" placeholder="aaaa-mm-dd">
							</div>
						</div>
					</div>

				</br>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<input type="submit"  value="Guardar" class="btn btn-success btn-block">
							<a href="{{ route('afiliados.index') }}" class="btn btn-info btn-block" >Atrás</a>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>
@endsection