@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-18">
			<div class="card" style="width:1200px;overflow-x:hidden;">
				@if (session('alert'))
				<div class="alert alert-success">
					{{ session('alert') }}
				</div>
				@endif
				<div class="card-header" align="center" style="  color: black;border-radius: 10px; height: 40px">
					<h4>{{ __('Afiliados') }}</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-sm-8"></div>
						<div class="col-sm">
							<div>
								<a class="btn-Top" href="{{ route('afiliados.create') }}">
									<span class="glyphicon glyphicon-plus"></span> Nuevo Afiliado
								</a>
							</div>
						</div>
					</div>

						<table id="tabla_afiliados" class="table table-bordered">
							<thead>
								<th>Nombre</th>
								<th>Telefono</th>
								<th>Ver</th>
								<th>Editar</th>
								<th>Eliminar</th>
							</thead>
							<tbody>
								@foreach($afiliados as $afiliado)
									<tr>
										<td>{{$afiliado->Nombre}}</td>
										<td>{{$afiliado->telefono}}</td>
										<td><a href="{{route('afiliados.show',$afiliado->id_afiliado)}}"> ver</a></td>
										<td><a href="{{route('afiliados.edit',$afiliado->id_afiliado)}}"> editar</a></td>
										<td><a href="{{route('afiliados.destroy',$afiliado->id_afiliado)}}"> eliminar</a></td>
									</tr>
								@endforeach
							</tbody>
						</table>
					<div align="center">
						{{$afiliados->render()}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection