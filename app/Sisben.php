<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sisben extends Model
{
    protected $table='sisbens';
    protected $primarykey='id_sisben';
    protected $fillable = ['nivel_sisben',
    'puntaje',
    'id_afiliado'
    ];
    public $timeStamps = false;

    // relacion muchos a uno
    public function Afiliado(){
        return $this->belongsTo('App\Afiliado');
    }
}
