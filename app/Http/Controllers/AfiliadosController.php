<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Afiliado;

class AfiliadosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $afiliados = Afiliado::orderBy('Nombre','Desc')->paginate(3);
        return view('afiliados.index')->with('afiliados',$afiliados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('afiliados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->get('tipo_identificacion'));
        $afiliado = new Afiliado();
        $afiliado->n_identificacion = $request->get('n_identificacion');
        $afiliado->Nombre = $request->get('nombre');
        $afiliado->tipo_documento = $request->get('tipo_identificacion');
        $afiliado->direccion = $request->get('direccion');
        $afiliado->telefono = $request->get('telefono');
        $afiliado->fecha_nacimiento = $request->get('fecha_nacimiento');
        $afiliado->fecha_afiliacion = $request->get('fecha_afiliacion');
        $afiliado->save();
        return redirect()->route('afiliados.index')->with('alert','resgistrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
