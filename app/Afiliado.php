<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Afiliado extends Model
{
    protected $table='afiliados';
    protected $primarykey='id_afiliado';
    protected $fillable = ['n_identificacion',
    'Nombre',
    'tipo_documento',
    'direccion',
    'telefono',
    'fecha_nacimiento',
    'fecha_afiliacion',
    ];
    public $timeStamps = false;

    // relacion muchos a uno
    public function Sisben(){
        return $this->hasMany('App\Sisben');
    }
}
