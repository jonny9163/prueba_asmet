<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/prueba',function(){
//     $afiliados = App\Afiliado::find(1);
//     dd($afiliados->nombre);
// });

Route::resource('/afiliados', AfiliadosController::class);

Route::resource('sisbens', SisbenController::class);

//mrutas de autenticacion
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
